import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import UserTable from "./usertable";
import Header from "./header";
import AddUser from "./adduser";

import Data from "./data";
//import nextId from 'react-id-generator'
//import nextId from "react-id-generator";
//import {UID} from 'react-uid';
import axios from "axios";
const shortid = require("shortid");

function App() {
  //const shortid = require('shortid')
  const [data, setData] = useState([]);
  useEffect(async () => {
    const result = await fetch("https://jsonplaceholder.typicode.com/posts");
    const json = await result.json();
    setData(json);
  }, []);
  const userData = [
    { id: 1, name: "Mohamad", username: "Mohamad" },
    { id: 2, name: "Ahmed", username: "Ahmed" },
    { id: 3, name: "Omar", username: "Omar" }
  ];
  const [users, setUsers] = useState(userData);
  const addUser = user => {
    //user.id = nextId()
    user.id = shortid.generate();

    setUsers([...users, user]);
  };
  return (
    <div className="container">
      <Header />
      <div>
        {/* <UserTable users={users} /> */}
        {/* <AddUser addUser={addUser} /> */}
      </div>
      {/* <div>
        {data.map(x => (
          <ul key={x.id}>
            <li>{x.title}</li>
          </ul>
        ))}
      </div>*/}
      {/* <Data x={data}></Data> */}
    </div>
  );
}

export default App;
