import React, { useState } from 'react';

function AddUser(props) {
	const inialFormState = { id: null, name: '', username: '' };
	const [ user, setUser ] = useState(inialFormState);
	const handleChange = (e) => {
		const { name, value } = e.target;
		setUser({ ...user, [name]: value });
	};
	return (
		<form
			onSubmit={(e) => {
				e.preventDefault();
				if (!user.name || !user.username) return;
				props.addUser(user);
				setUser(inialFormState)
			}}
		>
			<label>name</label>
			<input type="text" name="name" value={user.name} onChange={handleChange}  autoFocus/>
			<label>username</label>
			<input type="text" name="username" value={user.username} onChange={handleChange} />
			<button className="button is-primary is-small">AddUser</button>
		</form>
	);
}
export default AddUser;
