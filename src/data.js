import React from "react";
function Data(props) {
  return (
    <div>
      <hr />

      <hr />
      {props.x.map(x => (
        <ul key={x.id}>
          <li>
            <b>{x.title} </b>
            <button className=" button is-primary is-small">Delete</button>
            <p>{x.body}</p>
            <hr />
          </li>
        </ul>
      ))}
    </div>
  );
}

export default Data;
