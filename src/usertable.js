import React from 'react';
const UserTable = (props) => (
	<div>
		{props.users.length > 0 ? (
			props.users.map((user) => (
				<div key={user.id}>
					<ul>
						<li>
							- My name is <b>{user.name}</b> and my user name is {user.username} and my id is {user.id}
						</li>
					</ul>
				</div>
			))
		) : (
			<p>No Users!</p>
		)}
	</div>
);
export default UserTable;
